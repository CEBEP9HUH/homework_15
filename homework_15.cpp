﻿#include <iostream>

void printEvenOrOdd(int N, bool evens) noexcept
{
    for (auto i = evens ? 0 : 1; i < N; i+=2)
        std::cout << i << std::endl;
}

int main()
{
    const int intervalEnd = 50;
    printEvenOrOdd(intervalEnd, true);
    printEvenOrOdd(intervalEnd, false);
}   